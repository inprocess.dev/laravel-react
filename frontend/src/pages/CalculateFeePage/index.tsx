import React, { useEffect } from 'react';
import { useAppContext } from '../../contexts/AppContext';
import PageHeader from '../../components/PageHeader';
import PaymentCalculatorForm from '../../components/PaymentCalculatorForm'

const CalculateFeePage: React.FC = () => {
  const { setHeaderText } = useAppContext();

  useEffect(() => {
    setHeaderText('Calculate Fee');
  }, [setHeaderText]);

  return <div className='content'>
    <PageHeader text='Parking Payment Calculator' />
    <PaymentCalculatorForm/>
  </div>;
}

export default CalculateFeePage;