import React, { createContext, useContext, useState, ReactNode } from 'react';

interface AppContextType {
  headerText: string;
  setHeaderText: React.Dispatch<React.SetStateAction<string>>;
}

const AppContext = createContext<AppContextType | undefined>(undefined);

export const useAppContext = () => {
  const context = useContext(AppContext);
  if (context === undefined) {
    throw new Error('useAppContext must be used within a AppProvider');
  }
  return context;
};

interface AppProviderProps {
  children: ReactNode;
}

export const AppProvider: React.FC<AppProviderProps> = ({ children }) => {
  const [headerText, setHeaderText] = useState<string>('Default Header Text');

  const contextValue = {
    headerText,
    setHeaderText,
  };

  return (
    <AppContext.Provider value={contextValue}>
      {children}
    </AppContext.Provider>
  );
};