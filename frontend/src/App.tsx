import { AppProvider } from './contexts/AppContext';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Header from './components/Header';
import Navigation from './components/Navigation';
import Footer from './components/Footer';
import HomePage from './pages/HomePage';
import ManageParkingPage from './pages/ManageParkingPage';
import CalculateFeePage from './pages/CalculateFeePage';
import './App.css'

function App() {

  return (
    <AppProvider>
    <Router>
      <Header/>
      <Navigation />
      <Routes>
        <Route path="/" element={<HomePage />} />
        <Route path="/parking" element={<ManageParkingPage />} />
        <Route path="/payment" element={<CalculateFeePage />} />
      </Routes>
      <Footer />
    </Router>
  </AppProvider>
  )
}

export default App
