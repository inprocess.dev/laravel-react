import React, { useState, useEffect, useCallback, FormEvent } from 'react';
import { toast, ToastContainer } from "react-toastify";
import { useParkingAreas } from '../../hooks/useParkingAreas';
import FormField from '../FormField';
import SelectField from '../SelectField';
import SubmitButton from '../SubmitButton';
import PaymentResult from '../PaymentResult';
import { getCurrentTime, getCurrentDay, isDateValid } from '../../utils';
import { API_BASE_URL } from "../../constants";
import 'react-toastify/dist/ReactToastify.css';
import styles from './payment-calculator-form.module.css';

interface PaymentData {
    amount: number;
    originalAmount: number;
    discount: number;
}

const PaymentCalculatorForm: React.FC = () => {

    const currentDay = getCurrentDay();
    const currentTime = getCurrentTime();

    const { availableParkingAreas, fetchAvailableParkingAreas } = useParkingAreas();

    const [selectedParkingArea, setSelectedParkingArea] = useState<string>('');
    const [startTime, setStartTime] = useState<string>(currentTime);
    const [endTime, setEndTime] = useState<string>(currentTime);
    const [parkingDay, setParkingDay] = useState<string>(currentDay);
    const [currency, setCurrency] = useState<string>('USD');
    const [paymentCurrency, setPaymentCurrency] = useState<string>('USD');
    const [paymentResult, setPaymentResult] = useState<PaymentData | null>(null);

    useEffect(() => {
        fetchAvailableParkingAreas();  
        const intervalId = setInterval(() => {
            fetchAvailableParkingAreas();
        }, 2000);
        return () => clearInterval(intervalId);
    }, [fetchAvailableParkingAreas]);

    useEffect(() => {
        if (availableParkingAreas.length > 0) {
            setSelectedParkingArea(availableParkingAreas[0].id);
        }
    }, [availableParkingAreas]);


    const parkingAreaOptions = availableParkingAreas.map(area => ({
        value: area.id,
        text: `${area.parkingAreaName} - from $${area.weekdayRate}/hr`,
    }));

    const currencyOptions = [
        { value: 'USD', text: 'USD' },
        { value: 'EUR', text: 'EUR' },
        { value: 'PLN', text: 'PLN' },
    ];

    const handleInputChange = useCallback(
        (event: React.ChangeEvent<HTMLInputElement> | React.ChangeEvent<HTMLSelectElement>) => {
            const { name, value } = event.target;
            switch (name) {
                case 'selectedParkingArea':
                    setSelectedParkingArea(value);
                    break;
                case 'startTime':
                    setStartTime(value);
                    break;
                case 'endTime':
                    setEndTime(value);
                    break;
                case 'parkingDay':
                    setParkingDay(value);
                    break;
                case 'currency':
                    setCurrency(value);
                    break;
                default:
                    break;
            }
        }, []);

    const handleFormSubmit = (event: FormEvent<HTMLFormElement>) => {
        event.preventDefault();

        if (!isDateValid(parkingDay)) {
            toast.error('Parking for past dates is not allowed.');
            setPaymentResult(null);
            return;
        }

        const paymentData = {
            selectedParkingArea,
            startTime,
            endTime,
            parkingDay,
            currency,
        };

        fetch(`${API_BASE_URL}/calculate-payment`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            body: JSON.stringify(paymentData),
        })
            .then((response) => {
                if (!response.ok) {
                    throw new Error('Network response was not ok');
                }
                return response.json();
            })
            .then((data: PaymentData) => {
                setPaymentResult(data);
                setPaymentCurrency(currency);
            })
            .catch((error: Error) => {
                console.error('There has been a problem with your fetch operation:', error);
                toast.error("Error calculating payment.");
            });
    };

    const handleConfirmPayment = () => {
        const paymentData = {
          parkingAreaId: selectedParkingArea,
          startTime: `${parkingDay} ${startTime}`,
          endTime: `${parkingDay} ${endTime}`,
          parkingDay
        };
      
        fetch(`${API_BASE_URL}/parking-sessions`, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
          },
          body: JSON.stringify(paymentData),
        })
        .then(response => response.json())
        .then(data => {
            console.log('Success:', data);
            toast.success('Parking area has been successfully purchased.');
            fetchAvailableParkingAreas();
          })
          .catch((error) => {
            console.error('Error:', error);
            toast.error('An error occurred while purchasing the parking area.');
          });
      };


    return (
        <div>
            <ToastContainer />
            <form id="paymentCalculatorForm" onSubmit={handleFormSubmit} className={styles.formContainer}>
                <SelectField
                    id="selectedParkingArea"
                    label="Select Parking Area:"
                    name="selectedParkingArea"
                    required
                    selectedValue={selectedParkingArea}
                    onChange={handleInputChange}
                    options={parkingAreaOptions}
                />

                <FormField
                    label="Start Time:"
                    type="time"
                    id="startTime"
                    name="startTime"
                    value={startTime}
                    onChange={handleInputChange}
                    required />
                <FormField label="End Time:" type="time" id="endTime" name="endTime" onChange={handleInputChange} required />
                <FormField label="Parking Day:" type="date" id="parkingDay" name="parkingDay" value={parkingDay} onChange={handleInputChange} required />

                <SelectField
                    id="currency"
                    label="Currency:"
                    name="currency"
                    required
                    selectedValue={currency}
                    onChange={handleInputChange}
                    options={currencyOptions}
                />

                <SubmitButton id="calculatePayment" value="Calculate" />
            </form>
            {paymentResult && (
                <PaymentResult
                    amount={paymentResult.amount}
                    originalAmount={paymentResult.originalAmount}
                    discount={paymentResult.discount}
                    currency={paymentCurrency}
                    onConfirmPayment={handleConfirmPayment}
                />
            )}
        </div>


    );
};

export default PaymentCalculatorForm;