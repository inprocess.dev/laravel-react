import React from 'react';
import styles from './submit-button.module.css';

interface SubmitButtonProps {
  id: string;
  value: string;
  onClick?: () => void; // Добавляем необязательный пропс onClick
}

const SubmitButton: React.FC<SubmitButtonProps> = ({ id, value, onClick }) => {
  return (
    <button
      type="submit"
      id={id}
      className={styles.submitButton}
      onClick={onClick} // Используем пропс onClick
    >
      {value}
    </button>
  );
};

export default SubmitButton;