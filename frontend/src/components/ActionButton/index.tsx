import React, { ReactNode, MouseEventHandler } from 'react';
import styles from './action-button.module.css';

interface ActionButtonProps {
  actionType: 'edit' | 'delete';
  onClick: MouseEventHandler<HTMLButtonElement>;
  children: ReactNode;
}

const ActionButton: React.FC<ActionButtonProps> = ({ actionType, onClick, children }) => {
  const buttonClassName = `${styles.actionButton} ${actionType === 'edit' ? styles.editButton : styles.deleteButton
    }`;

  return (
    <button className={buttonClassName} onClick={onClick}>
      {children}
    </button>
  );
};

export default ActionButton;