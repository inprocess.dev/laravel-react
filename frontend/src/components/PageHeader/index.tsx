import React from 'react';
import styles from './page-header.module.css';

interface PageHeaderProps {
  text: string;
}

const PageHeader: React.FC<PageHeaderProps> = ({ text }) => {
  return <h2 className={styles.pageHeader}>{text}</h2>;
};

export default PageHeader;