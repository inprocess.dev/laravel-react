import React from 'react';
import SubmitButton from '../SubmitButton';
import styles from './payment-result.module.css';

interface PaymentResultProps {
  amount: number;
  originalAmount: number;
  discount: number;
  currency: string;
  onConfirmPayment?: () => void;
}

const PaymentResult: React.FC<PaymentResultProps> = ({ amount, originalAmount, discount, currency, onConfirmPayment }) => {
  const formatCurrency = (amount: number): string => {
    const numberAmount = amount.toFixed(2);
    switch (currency) {
      case 'USD':
        return `$${numberAmount}`;
      case 'EUR':
        return `€${numberAmount}`;
      case 'PLN':
        return `${numberAmount}zł`;
      default:
        return `$${numberAmount}`;
    }
  };

  const formattedAmount = formatCurrency(amount);
  const formattedOriginalAmount = formatCurrency(originalAmount);

  return (
    <div className={styles.paymentResult}>
      <div className={styles.priceDetails}>
        {discount > 0 ? (
          <>
            <del>Old Price: {formattedOriginalAmount}</del>
            <b>New Price: {formattedAmount}</b>
            <small>Discount: {discount}%</small>
          </>
        ) : (
          <b>Amount to pay: {formattedAmount}</b>
        )}
      </div>
      <div className={styles.submitButtonContainer}>
        <SubmitButton id="confirmPayment" value="Confirm Payment" onClick={onConfirmPayment}/>
      </div>
    </div>
  );
};

export default PaymentResult;