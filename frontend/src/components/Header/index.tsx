import React from 'react';
import { useAppContext } from '../../contexts/AppContext';
import styles from './header.module.css';

const Header: React.FC = () => {
  const { headerText } = useAppContext();

  return (
    <div className={styles.header}>
      <h1>{headerText}</h1>
    </div>
  );
};

export default Header;