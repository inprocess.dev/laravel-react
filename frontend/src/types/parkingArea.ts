export interface ParkingArea {
  id: string;
  parkingAreaName: string;
  weekdayRate: string;
  weekendRate: string;
  discountPercentage: number;
}