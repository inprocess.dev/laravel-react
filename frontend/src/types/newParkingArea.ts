export interface NewParkingArea {
  parkingAreaName: string;
  weekdayRate: string;
  weekendRate: string;
  discountPercentage: number;
}
