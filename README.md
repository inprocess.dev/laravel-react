# Parking Fee Calculator

## Overview

This web application is designed to manage parking areas and calculate parking fees for selected parking areas. The server side is built with PHP using the Laravel framework, while the client side is powered by React.js with TypeScript.

## Features

### Parking Areas Management (View 1)

- Create new parking areas.
- Edit existing parking areas.
- Delete parking areas.
- Each parking area has the following properties:
  - Parking-area name.
  - Weekday hourly rate (USD).
  - Weekend hourly rate (USD).
  - Discount percentage.

### Payment Calculation (View 2)

- User interface to calculate the amount to be paid for parking.
- Inputs:
  - Select a parking area from the available options.
  - Parking start time and end time.
  - Parking day.
  - Currency selection (USD by default, with options to convert to EUR and PLN).
- Outputs:
  - Calculated amount to pay.
- Currency exchange rates are fetched from the [ExchangeRatesAPI](https://exchangeratesapi.io) based on the payment date.

## Tech Stack

- Server side: PHP (Laravel)
- Client side: JavaScript (React.js with TypeScript)