<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('parking_areas', function (Blueprint $table) {
            $table->id();
            $table->string('parkingAreaName', 30);
            $table->decimal('weekdayRate', 10, 2);
            $table->decimal('weekendRate', 10, 2);
            $table->integer('discountPercentage');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('parking_areas');
    }
};
