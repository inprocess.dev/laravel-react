<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\ParkingSession;

class CleanExpiredSessions extends Command
{
    protected $signature = 'sessions:clean';
    protected $description = 'Clean up expired parking sessions';

    public function handle()
    {
        $now = new \DateTime('now', new \DateTimeZone('Europe/Warsaw'));
        $nowString = $now->format('Y-m-d H:i:s');

        $deletedRows = ParkingSession::where('endTime', '<', $nowString)->delete();

        $this->info("Deleted $deletedRows expired sessions.");
    }
}