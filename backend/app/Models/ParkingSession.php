<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ParkingSession extends Model
{
    use HasFactory;

    protected $fillable = ['parkingAreaId', 'startTime', 'endTime', 'parkingDay'];

    public function parkingArea()
    {
        return $this->belongsTo(ParkingArea::class, 'parkingAreaId');
    }

    public static function createSession($data)
    {
        return self::create([
            'parkingAreaId' => $data['parkingAreaId'],
            'startTime' => $data['startTime'],
            'endTime' => $data['endTime'],
            'parkingDay' => $data['parkingDay'],
        ]);
    }
}
