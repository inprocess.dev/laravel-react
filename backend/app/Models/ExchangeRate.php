<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ExchangeRate extends Model
{
    use HasFactory;

    protected $fillable = ['rateDate', 'USD', 'EUR', 'PLN'];

    protected $casts = [
        'rateDate' => 'date',
        'USD' => 'decimal:4',
        'EUR' => 'decimal:4',
        'PLN' => 'decimal:4',
    ];

    public static function getRateByDate(string $date): ?ExchangeRate
    {
        return self::where('rateDate', $date)->first();
    }

    public static function createRate(string $date, array $rates): ExchangeRate
    {
        return self::create([
            'rateDate' => $date,
            'USD' => $rates['USD'],
            'EUR' => $rates['EUR'],
            'PLN' => $rates['PLN'],
        ]);
    }
}
