<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ParkingArea extends Model
{
    use HasFactory;
    protected $fillable = ['parkingAreaName', 'weekdayRate', 'weekendRate', 'discountPercentage'];
    protected $hidden = ['created_at', 'updated_at'];


    public static function getAvailableParkingAreas()
    {
        return static::whereDoesntHave('parkingSessions', function ($query) {
            $now = new \DateTime('now', new \DateTimeZone('Europe/Warsaw'));
            $nowString = $now->format('Y-m-d H:i:s');
            
            $query->where('startTime', '<=', $nowString)
                  ->where('endTime', '>=', $nowString);
        })->get();
    }
 
     public function parkingSessions()
     {
         return $this->hasMany(ParkingSession::class, 'parkingAreaId');
     }
}
