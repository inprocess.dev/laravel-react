<?php

namespace App\Http\Controllers;

use App\Models\ParkingArea;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ParkingAreaController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $parkingAreas = ParkingArea::all();
        return response()->json($parkingAreas);
    }

    public function available()
    {
        $availableParkingAreas = ParkingArea::getAvailableParkingAreas();
        return response()->json($availableParkingAreas);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'parkingAreaName' => 'required',
            'weekdayRate' => 'required|numeric',
            'weekendRate' => 'required|numeric',
            'discountPercentage' => 'required|integer'
        ]);

        $parkingArea = ParkingArea::create($validatedData);
        return response()->json($parkingArea, 201);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $parkingArea = ParkingArea::findOrFail($id);
        return response()->json($parkingArea);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $parkingArea = ParkingArea::findOrFail($id);
        $parkingArea->update($request->all());
        return response()->json($parkingArea);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $deleted = ParkingArea::destroy($id);

        if ($deleted) {
            return response()->json(['success' => true, 'message' => 'Parking area deleted successfully.']);
        } else {
            return response()->json(['success' => false, 'message' => 'Parking area not found.'], 404);
        }
    }
}
