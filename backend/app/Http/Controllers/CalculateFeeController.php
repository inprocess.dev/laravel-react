<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\ExchangeRate;
use App\Models\ParkingArea;
use DateTime;
use Http;
use Illuminate\Http\Request;

class CalculateFeeController extends Controller
{
    public function calculatePayment(Request $request)
    {
        $validated = $request->validate([
            'selectedParkingArea' => 'required|integer',
            'startTime' => 'required|date_format:H:i',
            'endTime' => 'required|date_format:H:i',
            'parkingDay' => 'required|date',
            'currency' => 'required|string'
        ]);

        $parkingArea = ParkingArea::findOrFail($validated['selectedParkingArea']);
      
        $startDateTime = new DateTime($validated['parkingDay'] . ' ' . $validated['startTime']);
        $endDateTime = new DateTime($validated['parkingDay'] . ' ' . $validated['endTime']);

        if ($endDateTime < $startDateTime) {
            $endDateTime->modify('+1 day');
        }

        $interval = $startDateTime->diff($endDateTime);
        $time = $interval->h + ($interval->i / 60);

        $dayOfWeek = date('N', strtotime($validated['parkingDay']));
        $isWeekend = in_array($dayOfWeek, [6, 7]);

        $rate = $isWeekend ? $parkingArea->weekendRate : $parkingArea->weekdayRate;
        $cost = $rate * $time;

        if ($validated['currency'] !== 'USD') {
            $exchangeRates = $this->getExchangeRates();
            $cost = $cost * $exchangeRates[$validated['currency']];
        }

        $cost = round($cost, 2);
        $originalCost = $cost;

        if ($parkingArea->discountPercentage > 0) {
            $discount = ($cost * $parkingArea->discountPercentage) / 100;
            $discount = round($discount, 2);
            $cost -= $discount;
        }

        return response()->json(['amount' => $cost, 'originalAmount' => $originalCost, 'discount' => $parkingArea->discountPercentage]);
    }

    public function getExchangeRates()
    {
        $date = date('Y-m-d');
        $rate = ExchangeRate::getRateByDate($date);

        if (!$rate) {
            $apiKey = '395949df842173234027d833fc6b0589';
            $endpoint = 'http://api.exchangeratesapi.io/latest?access_key=' . $apiKey . '&symbols=USD,EUR,PLN';

            $response = Http::get($endpoint);

            if ($response->failed()) {
                abort(500, 'Failed to fetch data from exchange rates API.');
            }

            $responseData = $response->json();

            if (isset($responseData['error'])) {
                abort(500, 'Error occurred while fetching exchange rates.');
            }

            $rates = $responseData['rates'];

            // As the base parameter is not available in the free version of the API,
            // we need to convert EUR base rates to USD base rates manually.
            $usdBaseRates = [
                'USD' => 1,
                'EUR' => 1 / $rates['USD'],
                'PLN' => $rates['PLN'] / $rates['USD']
            ];

            ExchangeRate::createRate($date, $usdBaseRates);

            return $usdBaseRates;
        }

        return [
            'USD' => $rate['USD'],
            'EUR' => $rate['EUR'],
            'PLN' => $rate['PLN']
        ];
    }
}
