<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\ParkingSession;
use Illuminate\Http\Request;

class ParkingSessionController extends Controller
{
    public function store(Request $request)
    {
        $session = ParkingSession::createSession($request->all());

        return response()->json(['message' => 'Session saved successfully', 'session' => $session]);
    }
}
