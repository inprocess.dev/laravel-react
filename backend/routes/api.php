<?php

use App\Http\Controllers\CalculateFeeController;
use App\Http\Controllers\ParkingSessionController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ParkingAreaController;

Route::post('/calculate-payment', [CalculateFeeController::class, 'calculatePayment'])->name('calculatePayment');
Route::get('/parking-areas/available', [ParkingAreaController::class, 'available'])->name('available');
Route::post('/parking-sessions', [ParkingSessionController::class, 'store']);
Route::apiResource('parking-areas', ParkingAreaController::class);


